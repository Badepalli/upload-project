package com.lar;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.lara.model.Person;

public class Bootstrap {
	
	final static Logger log=Logger.getLogger(Bootstrap.class);

	public static void main(String[] args)
	{
		log.info("main method starts");
		
		if(log.isDebugEnabled()){
			log.info("DEBUG IS ENABLED");
		}
		else
		{
			log.info("DEBUG IS DISABLED");
			
		}
		
		
		log.debug("debug log before setting info");
		log.trace("debug log before setting info");

		log.setLevel(Level.INFO);
		
		log.debug("debug log after setting info");
		log.trace("debug log after setting info");

		if(log.isDebugEnabled()){
			log.info("DEBUG IS ENABLED");
		}
		else
		{
			log.info("DEBUG IS DISABLED");
			
		}
		
		Person p=new Person();
		p.setId((long) 1234444);
		p.setName("Lara");
		log.trace("hello world");
		

		
	}

}
